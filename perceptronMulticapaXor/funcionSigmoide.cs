﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perceptronMulticapaXor
{
    class funcionSigmoide
    {
        public double sigmoide(double resultadoFuncion) { 
            return 1.0 / (1 + Math.Pow(Math.E, (-1) * resultadoFuncion));
        }

    }
}
