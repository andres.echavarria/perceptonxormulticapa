﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perceptronMulticapaXor {
    class Program {
        static void Main(string[] args){
            //Creacion de los objectos 
            Random valoresPesos = new Random();
            capaOculta capaOculta = new capaOculta();
            funcionSigmoide sigmoideFuncion = new funcionSigmoide();
            backPropagation backPropagationFuncion = new backPropagation();
            //Matriz XOR
            int[,] entradas = { { 1, 1, 0 }, { 1, 0, 1 }, { 0, 1, 1 }, { 0, 0, 0 } };
            //Deficinicion de las variables 
            double errorDelta1 = 0, errorDelta2 = 0, errorDelta3 = 0, y1 = 0, y2 = 0, y3 = 0;
            int fila = 0, iteraciones = 1;
            //Recorrido de la matriz 
            Console.WriteLine("Tabla de XOR");
            while (fila <= 3 ){
                y1 = 0; y2 = 0; y3 = 0; errorDelta1 = 0; errorDelta2 = 0; errorDelta3 = 0; iteraciones = 0;

                //Asignacion de los valores aleatorios
                double w1x1   = valoresPesos.NextDouble();
                double w2x1   = valoresPesos.NextDouble();
                double w1x2   = valoresPesos.NextDouble();
                double w2x2   = valoresPesos.NextDouble();
                double w1y1   = valoresPesos.NextDouble();
                double w1y2   = valoresPesos.NextDouble();
                double wBias1 = valoresPesos.NextDouble();
                double wBias2 = valoresPesos.NextDouble();
                double wBias3 = valoresPesos.NextDouble();
                while (iteraciones <= 1000){
                    //Calculo de los resultados de las neuronas de la capa oculta 
                    y1 = capaOculta.calculoNeuronas(entradas, w1x1, w1x2, wBias1, fila);
                    y2 = capaOculta.calculoNeuronas(entradas, w2x1, w2x2, wBias2, fila);
                    //  Funcion de activacion sigmoide
                    y1 = sigmoideFuncion.sigmoide(y1);
                    y2 = sigmoideFuncion.sigmoide(y2);
                    //Calculo de la salida de la neurona 
                    y3 = capaOculta.calculoSalidaNeurona(y1, y2, w1y1, w1y2, wBias3);
                    //Calculo sigmoide a y3
                    y3 = sigmoideFuncion.sigmoide(y3);
                    //BackPropagation calcula el error de la neurona de salida 
                    errorDelta3 = backPropagationFuncion.errorSalida(y3, entradas, fila);
                    //Ajuste de los pesos de la neurona de salida 
                    w1y1 = backPropagationFuncion.ajustePesosSalida(w1y1,errorDelta3,y1);
                    w1y2 = backPropagationFuncion.ajustePesosSalida(w1y2, errorDelta3, y2);
                    wBias3 = wBias3 + errorDelta3;
                    //Calculo del error de las neuronas de capa oculta 
                    errorDelta1 = backPropagationFuncion.errorCapaOculta(y1,errorDelta3,w1y1);
                    errorDelta2 = backPropagationFuncion.errorCapaOculta(y2, errorDelta3, w1y2);
                    //Ajusta los pesos de la neurona de la capa olculta(neurona1)
                    w1x1 = backPropagationFuncion.ajustePesoCapaOculta(w1x1, errorDelta1, entradas, fila, 0);
                    w1x2 = backPropagationFuncion.ajustePesoCapaOculta(w1x2, errorDelta1, entradas, fila,1);
                    wBias1 = wBias1 + errorDelta1;
                    //Ajusta los pesos de la neurona de la capa olculta(neurona2)
                    w2x1 = backPropagationFuncion.ajustePesoCapaOculta(w2x1,  errorDelta2, entradas, fila, 0);
                    w2x2 = backPropagationFuncion.ajustePesoCapaOculta(w2x2 , errorDelta2, entradas, fila, 1);
                    wBias2 = wBias2 + errorDelta2;
                    iteraciones++;
                }
                //Imprime cada fila de arregloXor al terminar el proceso

                Console.WriteLine("" + (int)entradas[fila, 0] + "\tXOR\t" + (int)entradas[fila, 1] + "= " + (int)entradas[fila, 2] + "" + " Calculado = " + errorDelta3);
                fila++;

            }
            Console.ReadKey();


        }
    }
}
