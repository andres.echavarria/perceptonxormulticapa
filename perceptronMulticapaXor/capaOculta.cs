﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perceptronMulticapaXor
{
    class capaOculta {
        public double calculoNeuronas(int [,] entradas, double pesoEntradas1,double pesoEntradas2,double pesoBias,int fila){
          
            return (entradas[fila, 0] * pesoEntradas1) + (entradas[fila, 1] * pesoEntradas2) + (1 * pesoBias);

        }

        public double calculoSalidaNeurona(double y1, double y2,double w1y1 , double w1y2 ,double pesoBias) { 
        return (y1 * w1y1) + (y2 * w1y2) + (1 * pesoBias);
        }
    }
}
