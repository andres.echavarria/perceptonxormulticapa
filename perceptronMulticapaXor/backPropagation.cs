﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace perceptronMulticapaXor
{
    class backPropagation
    {
        double factorAprendizaje = 0.5;
        public double errorSalida(double y3 , int[,] entradas, int fila) {
            return (y3 * (1 - y3)) * (entradas[fila, 2] - y3);

        }

        public double ajustePesosSalida(double pesosSalida, double errorDelta,double resultadoY) {
        return pesosSalida + (factorAprendizaje * errorDelta * resultadoY);

        }

        public double errorCapaOculta(double y,double errorDelta, double pesoBias) { 
            return (y * (1 - y)) * errorDelta - pesoBias;

        }

        public double ajustePesoCapaOculta(double peso, double errorDelta, int[,] entradas, int fila,int columna) {
            return peso + (factorAprendizaje * errorDelta * entradas[fila, columna]);
        }

    }
}
